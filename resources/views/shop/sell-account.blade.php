@extends('layouts.master')

@section('title')
	Dang ki rao ban account tren website cua chung toi
@endsection

@section('header')
	@include('particals.header-v2')

@endsection

@section('nav-v2')
	@include('particals.nav-bar-v2')
@endsection

@section('content')

	<div id="content" class="site-content" tabindex="-1">
	    <div class="container">
	        <nav class="woocommerce-breadcrumb"><a href="https://demo2.chethemes.com/electro">Home</a><span class="delimiter"><i class="fa fa-angle-right"></i></span>Cart</nav>
	        <div id="primary" class="content-area">
	            <main id="main" class="site-main">
					<article class="page type-page status-publish hentry">
						<header class="entry-header"><h1 itemprop="name" class="entry-title">Thong tin account ban cung cap se duoc thay doi theo chinh sach</h1></header><!-- .entry-header -->
						{{-- <div class="container"> --}}
							<form enctype="multipart/form-data" action="#" class="checkout woocommerce-checkout" method="post" name="checkout">
								<div id="customer_details" class="col1-set">
									<div class="col-1">
										<div class="woocommerce-billing-fields">

											<h3 style="text-align: center;">Thong tin tai khoan</h3>

											<p id="user_name_field" class="form-row form-row form-row-wide"><label class="" for="username">Ten dang nhap  <abbr title="required" class="required">*</abbr></label><input type="text" value="" placeholder="" id="username" name="username" class="input-text "></p>

											<p id="user_password_field" class="form-row form-row form-row-wide validate-required validate-phone"><label class="" for="user_password">Password <abbr title="required" class="required">*</abbr></label><input type="text" value="" placeholder="" id="user_password" name="user_password" class="input-text "></p><div class="clear"></div>

											<p id="user_email_field" class="form-row form-row form-row-wide validate-required"><label class="" for="user_email">Email dang ki  <abbr title="required" class="required">*</abbr></label><input type="email" value="" placeholder="" id="user_email" name="user_email" class="input-text "></p><div class="clear"></div>


											<p id="user_phone_field" class="form-row form-row form-row-wide validate-required "><label class="" for="user_phone">So dien thoai dang ki <abbr title="required" class="required">*</abbr></label><input type="text" value="" placeholder="" id="user_phone" name="user_phone" class="input-text "></p><div class="clear"></div>

											<p id="description_field" class="form-row form-row form-row-wide validate-required"><label class="" for="description">Mo ta ve tai khoan cua ban <abbr title="required" class="required">*</abbr></label><textarea id="description" class="input-text" name="description" placeholder="Mo ta mot chut ve tai khoan nhu co bao nhieu skin, dang o rank nao ..."></textarea></p><div class="clear"></div>


											<p id="kind_of_payment_field" class="form-row terms wc-terms-and-conditions"><label class="" for="kind_of_payment">Chon phuong thuc thanh toan khi ban duoc tai khoan <abbr title="required" class="required">*</abbr></label>

									      		<label class="radio" style="padding-left: 5em;"><input type="radio" name="optradio" id="credit_card" value="1"> Thanh toan qua ngan hang</label>

									      		<label class="radio" style="padding-left: 5em;"><input type="radio" name="optradio" id="2" value="2"> Thanh toan qua the dien thoai</label>

											</p><div class="clear"></div>

											<p id="kind_of_payment_field" class="form-row form-row form-row-wide validate-required"><label class="" for="kind_of_payment"> Tra phi quang cao de len trang chu </label>
												<label class="radio" style="padding-left: 5em;"><input type="radio" name="optradio" id="1" value="1"> Tra bang tien</label>
								      			<label class="radio" style="padding-left: 5em;"><input type="radio" name="optradio" id="2" value="2"> Thanh toan bang phi (% sau khi ban tai khoan)</label>

											</p>

											<p id="price_hope_field" class="form-row form-row form-row-wide validate-required"><label class="" for="price_hope">Gia ban muon ban  <abbr title="required" class="required">*</abbr></label><input type="text" value="" placeholder="" id="price_hope" name="price_hope" class="input-text "></p><div class="clear"></div>

										</div>
									</div>

								</div>

								<div class="woocommerce-checkout-review-order" id="order_review">
									<div class="woocommerce-checkout-payment" id="payment">
										<div class="form-row place-order">

										    <p class="form-row terms wc-terms-and-conditions">
												<input type="checkbox" id="terms" name="terms" class="input-checkbox">
										        <label class="checkbox" for="terms">I’ve read and accept the <a target="_blank" href="{{ route('condition') }}">terms &amp; conditions</a> <span class="required">*</span></label>
										        <input type="hidden" value="1" name="terms-field">
										    </p>

											<input type="submit" data-value="Place order" value="Ban" class="button alt">
										</div>
									</div>
								</div>
							</form>
						{{-- </div> --}}
					</article>
	            </main>
	        </div>
        </div>
	</div>
@endsection()

@section('footer')
	@include('particals.footer')
@endsection()