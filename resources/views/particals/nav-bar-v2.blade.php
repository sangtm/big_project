<nav class="navbar navbar-primary navbar-full hidden-md-down">
	    <div class="container">
	        <ul class="nav navbar-nav departments-menu animate-dropdown">
	            <li class="nav-item dropdown">
	                <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" id="departments-menu-toggle">
	                    Shop By Department              </a>
	                <ul id="menu-vertical-menu" class="dropdown-menu yamm departments-menu-dropdown" style="">
	                    <li id="menu-item-3211" class="highlight menu-item menu-item-type-post_type menu-item-object-page animate-dropdown menu-item-3211"><a title="Value of the Day" href="https://demo2.chethemes.com/electro/home-v2/">Value of the Day</a></li>
	                    <li id="menu-item-3212" class="highlight menu-item menu-item-type-post_type menu-item-object-page animate-dropdown menu-item-3212"><a title="Top 100 Offers" href="https://demo2.chethemes.com/electro/home-v3/">Top 100 Offers</a></li>
	                    <li id="menu-item-3213" class="highlight menu-item menu-item-type-post_type menu-item-object-page animate-dropdown menu-item-3213"><a title="New Arrivals" href="https://demo2.chethemes.com/electro/home-v3-full-color-background/">New Arrivals</a></li>
	                    <li id="menu-item-3071" class="yamm-tfw menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-has-children animate-dropdown menu-item-3071 dropdown-submenu"><a title="Computers &amp; Accessories" href="https://demo2.chethemes.com/electro/product-category/laptops-computers/" data-toggle="dropdown-hover" class="dropdown-toggle" aria-haspopup="true">Computers &amp; Accessories</a>
	                        <ul role="menu" class=" dropdown-menu" style="min-height: 747.004px;">
	                            <li id="menu-item-3218" class="menu-item menu-item-type-post_type menu-item-object-static_block animate-dropdown menu-item-3218" style="min-height: 743.004px;">
	                                <div class="yamm-content">
	                                    <div class="vc_row wpb_row vc_row-fluid bg-yamm-content bg-yamm-content-bottom bg-yamm-content-right">
	                                        <div class="wpb_column vc_column_container vc_col-sm-12">
	                                            <div class="vc_column-inner ">
	                                                <div class="wpb_wrapper">
	                                                    <div class="wpb_single_image wpb_content_element vc_align_left">
	                                                        <figure class="wpb_wrapper vc_figure">
	                                                            <div class="vc_single_image-wrapper   vc_box_border_grey"><img width="540" height="460" src="https://demo2.chethemes.com/electro/wp-content/uploads/2016/03/megamenu-2.png" class="vc_single_image-img attachment-full" alt="" srcset="https://demo2.chethemes.com/electro/wp-content/uploads/2016/03/megamenu-2.png 540w, https://demo2.chethemes.com/electro/wp-content/uploads/2016/03/megamenu-2-300x256.png 300w" sizes="(max-width: 540px) 100vw, 540px"></div>
	                                                        </figure>
	                                                    </div>
	                                                </div>
	                                            </div>
	                                        </div>
	                                    </div>
	                                    <div class="vc_row wpb_row vc_row-fluid">
	                                        <div class="wpb_column vc_column_container vc_col-sm-6">
	                                            <div class="vc_column-inner ">
	                                                <div class="wpb_wrapper">
	                                                    <div class="wpb_text_column wpb_content_element ">
	                                                        <div class="wpb_wrapper">
	                                                            <ul>
	                                                                <li class="nav-title">Computers &amp; Accessories</li>
	                                                                <li><a href="#">All Computers &amp; Accessories</a></li>
	                                                                <li><a href="#">Laptops, Desktops &amp; Monitors</a></li>
	                                                                <li><a href="#">Pen Drives, Hard Drives &amp; Memory Cards</a></li>
	                                                                <li><a href="#">Printers &amp; Ink</a></li>
	                                                                <li><a href="#">Networking &amp; Internet Devices</a></li>
	                                                                <li><a href="#">Computer Accessories</a></li>
	                                                                <li><a href="#">Software</a></li>
	                                                                <li class="nav-divider"></li>
	                                                                <li><a href="#"><span class="nav-text">All Electronics</span><span class="nav-subtext">Discover more products</span></a></li>
	                                                            </ul>
	                                                        </div>
	                                                    </div>
	                                                </div>
	                                            </div>
	                                        </div>
	                                        <div class="wpb_column vc_column_container vc_col-sm-6">
	                                            <div class="vc_column-inner ">
	                                                <div class="wpb_wrapper">
	                                                    <div class="wpb_text_column wpb_content_element ">
	                                                        <div class="wpb_wrapper">
	                                                            <ul>
	                                                                <li class="nav-title">Office &amp; Stationery</li>
	                                                                <li><a href="#">All Office &amp; Stationery</a></li>
	                                                                <li><a href="#">Pens &amp; Writing</a></li>
	                                                            </ul>
	                                                        </div>
	                                                    </div>
	                                                </div>
	                                            </div>
	                                        </div>
	                                    </div>
	                                </div>
	                            </li>
	                        </ul>
	                    </li>
	                    <li id="menu-item-3072" class="yamm-tfw menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-has-children animate-dropdown menu-item-3072 dropdown-submenu"><a title="Cameras, Audio &amp; Video" href="https://demo2.chethemes.com/electro/product-category/cameras-photography/" data-toggle="dropdown-hover" class="dropdown-toggle" aria-haspopup="true">Cameras, Audio &amp; Video</a>
	                        <ul role="menu" class=" dropdown-menu" style="min-height: 747.004px;">
	                            <li id="menu-item-3217" class="menu-item menu-item-type-post_type menu-item-object-static_block animate-dropdown menu-item-3217" style="min-height: 743.004px;">
	                                <div class="yamm-content">
	                                    <div class="vc_row wpb_row vc_row-fluid bg-yamm-content">
	                                        <div class="wpb_column vc_column_container vc_col-sm-12">
	                                            <div class="vc_column-inner ">
	                                                <div class="wpb_wrapper">
	                                                    <div class="wpb_single_image wpb_content_element vc_align_left">
	                                                        <figure class="wpb_wrapper vc_figure">
	                                                            <div class="vc_single_image-wrapper   vc_box_border_grey"><img width="540" height="460" src="https://demo2.chethemes.com/electro/wp-content/uploads/2016/03/megamenu-3.png" class="vc_single_image-img attachment-full" alt="" srcset="https://demo2.chethemes.com/electro/wp-content/uploads/2016/03/megamenu-3.png 540w, https://demo2.chethemes.com/electro/wp-content/uploads/2016/03/megamenu-3-300x256.png 300w" sizes="(max-width: 540px) 100vw, 540px"></div>
	                                                        </figure>
	                                                    </div>
	                                                </div>
	                                            </div>
	                                        </div>
	                                    </div>
	                                    <div class="vc_row wpb_row vc_row-fluid">
	                                        <div class="wpb_column vc_column_container vc_col-sm-6">
	                                            <div class="vc_column-inner ">
	                                                <div class="wpb_wrapper">
	                                                    <div class="wpb_text_column wpb_content_element ">
	                                                        <div class="wpb_wrapper">
	                                                            <ul>
	                                                                <li class="nav-title"><a href="#">Cameras &amp; Photography</a></li>
	                                                                <li><a href="#">All Cameras &amp; Photography</a></li>
	                                                                <li><a href="#">Digital SLRs</a></li>
	                                                                <li><a href="#">Point &amp; Shoot Cameras</a></li>
	                                                                <li><a href="#">Lenses</a></li>
	                                                                <li><a href="#">Camera Accessories</a></li>
	                                                                <li><a href="#">Security &amp; Surveillance</a></li>
	                                                                <li><a href="#">Binoculars &amp; Telescopes</a></li>
	                                                                <li><a href="#">Camcorders</a></li>
	                                                                <li class="nav-divider"></li>
	                                                                <li><a href="#"><span class="nav-text">All Electronics</span><span class="nav-subtext">Discover more products</span></a></li>
	                                                            </ul>
	                                                        </div>
	                                                    </div>
	                                                </div>
	                                            </div>
	                                        </div>
	                                        <div class="wpb_column vc_column_container vc_col-sm-6">
	                                            <div class="vc_column-inner ">
	                                                <div class="wpb_wrapper">
	                                                    <div class="wpb_text_column wpb_content_element ">
	                                                        <div class="wpb_wrapper">
	                                                            <ul>
	                                                                <li class="nav-title">Audio &amp; Video</li>
	                                                                <li><a href="#">All Audio &amp; Video</a></li>
	                                                                <li><a href="#">Headphones &amp; Speakers</a></li>
	                                                                <li><a href="#">Home Entertainment Systems</a></li>
	                                                                <li><a href="#">MP3 &amp; Media Players</a></li>
	                                                            </ul>
	                                                        </div>
	                                                    </div>
	                                                </div>
	                                            </div>
	                                        </div>
	                                    </div>
	                                </div>
	                            </li>
	                        </ul>
	                    </li>
	                    <li id="menu-item-3073" class="yamm-tfw menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-has-children animate-dropdown menu-item-3073 dropdown-submenu"><a title="Mobiles &amp; Tablets" href="https://demo2.chethemes.com/electro/product-category/smart-phones-tablets/" data-toggle="dropdown-hover" class="dropdown-toggle" aria-haspopup="true">Mobiles &amp; Tablets</a>
	                        <ul role="menu" class=" dropdown-menu" style="min-height: 747.004px;">
	                            <li id="menu-item-3219" class="menu-item menu-item-type-post_type menu-item-object-static_block animate-dropdown menu-item-3219" style="min-height: 743.004px;">
	                                <div class="yamm-content">
	                                    <div class="vc_row wpb_row vc_row-fluid bg-yamm-content">
	                                        <div class="wpb_column vc_column_container vc_col-sm-12">
	                                            <div class="vc_column-inner ">
	                                                <div class="wpb_wrapper">
	                                                    <div class="wpb_single_image wpb_content_element vc_align_left   bg-yamm-extend-outside">
	                                                        <figure class="wpb_wrapper vc_figure">
	                                                            <div class="vc_single_image-wrapper   vc_box_border_grey"><img width="540" height="495" src="https://demo2.chethemes.com/electro/wp-content/uploads/2016/03/megamenu-.png" class="vc_single_image-img attachment-full" alt="" srcset="https://demo2.chethemes.com/electro/wp-content/uploads/2016/03/megamenu-.png 540w, https://demo2.chethemes.com/electro/wp-content/uploads/2016/03/megamenu--300x275.png 300w" sizes="(max-width: 540px) 100vw, 540px"></div>
	                                                        </figure>
	                                                    </div>
	                                                </div>
	                                            </div>
	                                        </div>
	                                    </div>
	                                    <div class="vc_row wpb_row vc_row-fluid">
	                                        <div class="wpb_column vc_column_container vc_col-sm-6">
	                                            <div class="vc_column-inner ">
	                                                <div class="wpb_wrapper">
	                                                    <div class="wpb_text_column wpb_content_element ">
	                                                        <div class="wpb_wrapper">
	                                                            <ul>
	                                                                <li class="nav-title">Mobiles &amp; Tablets</li>
	                                                                <li><a href="#">All Mobile Phones</a></li>
	                                                                <li><a href="#">Smartphones</a></li>
	                                                                <li><a href="#">Android Mobiles</a></li>
	                                                                <li><a href="#">Windows Mobiles</a></li>
	                                                                <li><a href="#">Refurbished Mobiles</a></li>
	                                                                <li class="nav-divider"></li>
	                                                                <li><a href="#">All Mobile Accessories</a></li>
	                                                                <li><a href="#">Cases &amp; Covers</a></li>
	                                                                <li><a href="#">Screen Protectors</a></li>
	                                                                <li><a href="#">Power Banks</a></li>
	                                                                <li class="nav-divider"></li>
	                                                                <li><a href="#"><span class="nav-text">All Electronics</span><span class="nav-subtext">Discover more products</span></a></li>
	                                                            </ul>
	                                                        </div>
	                                                    </div>
	                                                </div>
	                                            </div>
	                                        </div>
	                                        <div class="wpb_column vc_column_container vc_col-sm-6">
	                                            <div class="vc_column-inner ">
	                                                <div class="wpb_wrapper">
	                                                    <div class="wpb_text_column wpb_content_element ">
	                                                        <div class="wpb_wrapper">
	                                                            <ul>
	                                                                <li class="nav-title"></li>
	                                                                <li><a href="#">All Tablets</a></li>
	                                                                <li><a href="#">Tablet Accessories</a></li>
	                                                                <li><a href="#">Landline Phones</a></li>
	                                                                <li><a href="#">Wearable Devices</a></li>
	                                                            </ul>
	                                                        </div>
	                                                    </div>
	                                                </div>
	                                            </div>
	                                        </div>
	                                    </div>
	                                </div>
	                            </li>
	                        </ul>
	                    </li>
	                    <li id="menu-item-3074" class="yamm-tfw menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-has-children animate-dropdown menu-item-3074 dropdown-submenu"><a title="Movies, Music &amp; Video Games" href="https://demo2.chethemes.com/electro/product-category/video-games-consoles/" data-toggle="dropdown-hover" class="dropdown-toggle" aria-haspopup="true">Movies, Music &amp; Video Games</a>
	                        <ul role="menu" class=" dropdown-menu" style="min-height: 747.004px;">
	                            <li id="menu-item-3220" class="menu-item menu-item-type-post_type menu-item-object-static_block animate-dropdown menu-item-3220" style="min-height: 743.004px;">
	                                <div class="yamm-content">
	                                    <div class="vc_row wpb_row vc_row-fluid bg-yamm-content">
	                                        <div class="wpb_column vc_column_container vc_col-sm-12">
	                                            <div class="vc_column-inner ">
	                                                <div class="wpb_wrapper">
	                                                    <div class="wpb_single_image wpb_content_element vc_align_left">
	                                                        <figure class="wpb_wrapper vc_figure">
	                                                            <div class="vc_single_image-wrapper   vc_box_border_grey"><img width="540" height="485" src="https://demo2.chethemes.com/electro/wp-content/uploads/2016/03/megamenu-8.png" class="vc_single_image-img attachment-full" alt="" srcset="https://demo2.chethemes.com/electro/wp-content/uploads/2016/03/megamenu-8.png 540w, https://demo2.chethemes.com/electro/wp-content/uploads/2016/03/megamenu-8-300x269.png 300w" sizes="(max-width: 540px) 100vw, 540px"></div>
	                                                        </figure>
	                                                    </div>
	                                                </div>
	                                            </div>
	                                        </div>
	                                    </div>
	                                    <div class="vc_row wpb_row vc_row-fluid">
	                                        <div class="wpb_column vc_column_container vc_col-sm-6">
	                                            <div class="vc_column-inner ">
	                                                <div class="wpb_wrapper">
	                                                    <div class="wpb_text_column wpb_content_element ">
	                                                        <div class="wpb_wrapper">
	                                                            <ul>
	                                                                <li class="nav-title">Movies &amp; TV Shows</li>
	                                                                <li><a href="#">All Movies &amp; TV Shows</a></li>
	                                                                <li><a href="#">Blu-ray</a></li>
	                                                                <li><a href="#">All English</a></li>
	                                                                <li><a href="#">All Hindi</a></li>
	                                                                <li class="nav-divider"></li>
	                                                                <li class="nav-title">Video Games</li>
	                                                                <li><a href="#">All Consoles, Games &amp; Accessories</a></li>
	                                                                <li><a href="#">PC Games</a></li>
	                                                                <li><a href="#">Pre-orders &amp; New Releases</a></li>
	                                                                <li><a href="#">Consoles</a></li>
	                                                                <li><a href="#">Accessories</a></li>
	                                                            </ul>
	                                                        </div>
	                                                    </div>
	                                                </div>
	                                            </div>
	                                        </div>
	                                        <div class="wpb_column vc_column_container vc_col-sm-6">
	                                            <div class="vc_column-inner ">
	                                                <div class="wpb_wrapper">
	                                                    <div class="wpb_text_column wpb_content_element ">
	                                                        <div class="wpb_wrapper">
	                                                            <ul>
	                                                                <li class="nav-title">Music</li>
	                                                                <li><a href="#">All Music</a></li>
	                                                                <li><a href="#">International Music</a></li>
	                                                                <li><a href="#">Film Songs</a></li>
	                                                                <li><a href="#">Indian Classical</a></li>
	                                                                <li><a href="#">Musical Instruments</a></li>
	                                                            </ul>
	                                                        </div>
	                                                    </div>
	                                                </div>
	                                            </div>
	                                        </div>
	                                    </div>
	                                </div>
	                            </li>
	                        </ul>
	                    </li>
	                    <li id="menu-item-3075" class="yamm-tfw menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-has-children animate-dropdown menu-item-3075 dropdown-submenu"><a title="TV &amp; Audio" href="https://demo2.chethemes.com/electro/product-category/tv-audio/" data-toggle="dropdown-hover" class="dropdown-toggle" aria-haspopup="true">TV &amp; Audio</a>
	                        <ul role="menu" class=" dropdown-menu" style="min-height: 747.004px;">
	                            <li id="menu-item-3221" class="menu-item menu-item-type-post_type menu-item-object-static_block animate-dropdown menu-item-3221" style="min-height: 743.004px;">
	                                <div class="yamm-content">
	                                    <div class="vc_row wpb_row vc_row-fluid bg-yamm-content">
	                                        <div class="wpb_column vc_column_container vc_col-sm-12">
	                                            <div class="vc_column-inner ">
	                                                <div class="wpb_wrapper">
	                                                    <div class="wpb_single_image wpb_content_element vc_align_left">
	                                                        <figure class="wpb_wrapper vc_figure">
	                                                            <div class="vc_single_image-wrapper   vc_box_border_grey"><img width="540" height="460" src="https://demo2.chethemes.com/electro/wp-content/uploads/2016/03/megamenu-4.png" class="vc_single_image-img attachment-full" alt="" srcset="https://demo2.chethemes.com/electro/wp-content/uploads/2016/03/megamenu-4.png 540w, https://demo2.chethemes.com/electro/wp-content/uploads/2016/03/megamenu-4-300x256.png 300w" sizes="(max-width: 540px) 100vw, 540px"></div>
	                                                        </figure>
	                                                    </div>
	                                                </div>
	                                            </div>
	                                        </div>
	                                    </div>
	                                    <div class="vc_row wpb_row vc_row-fluid">
	                                        <div class="wpb_column vc_column_container vc_col-sm-6">
	                                            <div class="vc_column-inner ">
	                                                <div class="wpb_wrapper">
	                                                    <div class="wpb_text_column wpb_content_element ">
	                                                        <div class="wpb_wrapper">
	                                                            <ul>
	                                                                <li class="nav-title">Audio &amp; Video</li>
	                                                                <li><a href="#">All Audio &amp; Video</a></li>
	                                                                <li><a href="#">Televisions</a></li>
	                                                                <li><a href="#">Headphones</a></li>
	                                                                <li><a href="#">Speakers</a></li>
	                                                                <li><a href="#">Home Entertainment Systems</a></li>
	                                                                <li><a href="#">MP3 &amp; Media Players</a></li>
	                                                                <li><a href="#">Audio &amp; Video Accessories</a></li>
	                                                                <li class="nav-divider"></li>
	                                                                <li><a href="#"><span class="nav-text">Electro Home Appliances</span><span class="nav-subtext">Available in select cities</span></a></li>
	                                                            </ul>
	                                                        </div>
	                                                    </div>
	                                                </div>
	                                            </div>
	                                        </div>
	                                        <div class="wpb_column vc_column_container vc_col-sm-6">
	                                            <div class="vc_column-inner ">
	                                                <div class="wpb_wrapper">
	                                                    <div class="wpb_text_column wpb_content_element ">
	                                                        <div class="wpb_wrapper">
	                                                            <ul>
	                                                                <li class="nav-title">Music</li>
	                                                                <li><a href="#">Televisions</a></li>
	                                                                <li><a href="#">Headphones</a></li>
	                                                                <li><a href="#">Speakers</a></li>
	                                                                <li><a href="#">Media Players</a></li>
	                                                            </ul>
	                                                        </div>
	                                                    </div>
	                                                </div>
	                                            </div>
	                                        </div>
	                                    </div>
	                                </div>
	                            </li>
	                        </ul>
	                    </li>
	                    <li id="menu-item-3076" class="yamm-tfw menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-has-children animate-dropdown menu-item-3076 dropdown-submenu"><a title="Watches &amp; Eyewear" href="https://demo2.chethemes.com/electro/product-category/gadgets/" data-toggle="dropdown-hover" class="dropdown-toggle" aria-haspopup="true">Watches &amp; Eyewear</a>
	                        <ul role="menu" class=" dropdown-menu" style="min-height: 747.004px;">
	                            <li id="menu-item-3222" class="menu-item menu-item-type-post_type menu-item-object-static_block animate-dropdown menu-item-3222" style="min-height: 743.004px;">
	                                <div class="yamm-content">
	                                    <div class="vc_row wpb_row vc_row-fluid bg-yamm-content">
	                                        <div class="wpb_column vc_column_container vc_col-sm-12">
	                                            <div class="vc_column-inner ">
	                                                <div class="wpb_wrapper">
	                                                    <div class="wpb_single_image wpb_content_element vc_align_left">
	                                                        <figure class="wpb_wrapper vc_figure">
	                                                            <div class="vc_single_image-wrapper   vc_box_border_grey"><img width="540" height="486" src="https://demo2.chethemes.com/electro/wp-content/uploads/2016/03/megamenu-7.png" class="vc_single_image-img attachment-full" alt="" srcset="https://demo2.chethemes.com/electro/wp-content/uploads/2016/03/megamenu-7.png 540w, https://demo2.chethemes.com/electro/wp-content/uploads/2016/03/megamenu-7-300x270.png 300w" sizes="(max-width: 540px) 100vw, 540px"></div>
	                                                        </figure>
	                                                    </div>
	                                                </div>
	                                            </div>
	                                        </div>
	                                    </div>
	                                    <div class="vc_row wpb_row vc_row-fluid">
	                                        <div class="wpb_column vc_column_container vc_col-sm-6">
	                                            <div class="vc_column-inner ">
	                                                <div class="wpb_wrapper">
	                                                    <div class="wpb_text_column wpb_content_element ">
	                                                        <div class="wpb_wrapper">
	                                                            <ul>
	                                                                <li class="nav-title">Watches</li>
	                                                                <li><a href="#">All Watches</a></li>
	                                                                <li><a href="#">Men’s Watches</a></li>
	                                                                <li><a href="#">Women’s Watches</a></li>
	                                                                <li><a href="#">Premium Watches</a></li>
	                                                                <li><a href="#">Deals on Watches</a></li>
	                                                            </ul>
	                                                        </div>
	                                                    </div>
	                                                </div>
	                                            </div>
	                                        </div>
	                                        <div class="wpb_column vc_column_container vc_col-sm-6">
	                                            <div class="vc_column-inner ">
	                                                <div class="wpb_wrapper">
	                                                    <div class="wpb_text_column wpb_content_element ">
	                                                        <div class="wpb_wrapper">
	                                                            <ul>
	                                                                <li class="nav-title">Eyewear</li>
	                                                                <li><a href="#">Men’s Sunglasses</a></li>
	                                                                <li><a href="#">Women’s Sunglasses</a></li>
	                                                                <li><a href="#">Spectacle Frames</a></li>
	                                                                <li><a href="#">All Sunglasses</a></li>
	                                                                <li><a href="#">Amazon Fashion</a></li>
	                                                            </ul>
	                                                        </div>
	                                                    </div>
	                                                </div>
	                                            </div>
	                                        </div>
	                                    </div>
	                                </div>
	                            </li>
	                        </ul>
	                    </li>
	                    <li id="menu-item-3077" class="yamm-tfw menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-has-children animate-dropdown menu-item-3077 dropdown-submenu"><a title="Car, Motorbike &amp; Industrial" href="https://demo2.chethemes.com/electro/product-category/car-electronic-gps/" data-toggle="dropdown-hover" class="dropdown-toggle" aria-haspopup="true">Car, Motorbike &amp; Industrial</a>
	                        <ul role="menu" class=" dropdown-menu" style="min-height: 747.004px;">
	                            <li id="menu-item-3223" class="menu-item menu-item-type-post_type menu-item-object-static_block animate-dropdown menu-item-3223" style="min-height: 743.004px;">
	                                <div class="yamm-content">
	                                    <div class="vc_row wpb_row vc_row-fluid bg-yamm-content">
	                                        <div class="wpb_column vc_column_container vc_col-sm-12">
	                                            <div class="vc_column-inner ">
	                                                <div class="wpb_wrapper">
	                                                    <div class="wpb_single_image wpb_content_element vc_align_left">
	                                                        <figure class="wpb_wrapper vc_figure">
	                                                            <div class="vc_single_image-wrapper   vc_box_border_grey"><img width="540" height="523" src="https://demo2.chethemes.com/electro/wp-content/uploads/2016/03/megamenu-9.png" class="vc_single_image-img attachment-full" alt="" srcset="https://demo2.chethemes.com/electro/wp-content/uploads/2016/03/megamenu-9.png 540w, https://demo2.chethemes.com/electro/wp-content/uploads/2016/03/megamenu-9-300x291.png 300w" sizes="(max-width: 540px) 100vw, 540px"></div>
	                                                        </figure>
	                                                    </div>
	                                                </div>
	                                            </div>
	                                        </div>
	                                    </div>
	                                    <div class="vc_row wpb_row vc_row-fluid">
	                                        <div class="wpb_column vc_column_container vc_col-sm-6">
	                                            <div class="vc_column-inner ">
	                                                <div class="wpb_wrapper">
	                                                    <div class="wpb_text_column wpb_content_element ">
	                                                        <div class="wpb_wrapper">
	                                                            <ul>
	                                                                <li class="nav-title">Car &amp; Motorbike</li>
	                                                                <li><a href="#">All Cars &amp; Bikes</a></li>
	                                                                <li><a href="#">Car &amp; Bike Care</a></li>
	                                                                <li><a href="#">Lubricants</a></li>
	                                                                <li class="nav-divider"></li>
	                                                                <li class="nav-title">Shop for Bike</li>
	                                                                <li><a href="#">Helmets &amp; Gloves</a></li>
	                                                                <li><a href="#">Bike Parts</a></li>
	                                                                <li class="nav-title">Shop for Car</li>
	                                                                <li><a href="#">Air Fresheners</a></li>
	                                                                <li><a href="#">Car Parts</a></li>
	                                                                <li><a href="#">Tyre Accessories</a></li>
	                                                            </ul>
	                                                        </div>
	                                                    </div>
	                                                </div>
	                                            </div>
	                                        </div>
	                                        <div class="wpb_column vc_column_container vc_col-sm-6">
	                                            <div class="vc_column-inner ">
	                                                <div class="wpb_wrapper">
	                                                    <div class="wpb_text_column wpb_content_element ">
	                                                        <div class="wpb_wrapper">
	                                                            <ul>
	                                                                <li class="nav-title">Industrial Supplies</li>
	                                                                <li><a href="#">All Industrial Supplies</a></li>
	                                                                <li><a href="#">Lab &amp; Scientific</a></li>
	                                                                <li><a href="#">Janitorial &amp; Sanitation Supplies</a></li>
	                                                                <li><a href="#">Test, Measure &amp; Inspect</a></li>
	                                                            </ul>
	                                                        </div>
	                                                    </div>
	                                                </div>
	                                            </div>
	                                        </div>
	                                    </div>
	                                </div>
	                            </li>
	                        </ul>
	                    </li>
	                    <li id="menu-item-3078" class="menu-item menu-item-type-taxonomy menu-item-object-product_cat animate-dropdown menu-item-3078"><a title="Accessories" href="https://demo2.chethemes.com/electro/product-category/laptops-computers/accessories-laptops-computers/">Accessories</a></li>
	                    <li id="menu-item-3079" class="menu-item menu-item-type-taxonomy menu-item-object-product_cat animate-dropdown menu-item-3079"><a title="Printers &amp; Ink" href="https://demo2.chethemes.com/electro/product-category/printers-ink/">Printers &amp; Ink</a></li>
	                    <li id="menu-item-3080" class="menu-item menu-item-type-taxonomy menu-item-object-product_cat animate-dropdown menu-item-3080"><a title="Software" href="https://demo2.chethemes.com/electro/product-category/software/">Software</a></li>
	                    <li id="menu-item-3081" class="menu-item menu-item-type-taxonomy menu-item-object-product_cat animate-dropdown menu-item-3081"><a title="Office Supplies" href="https://demo2.chethemes.com/electro/product-category/office-supplies/">Office Supplies</a></li>
	                    <li id="menu-item-3082" class="menu-item menu-item-type-taxonomy menu-item-object-product_cat animate-dropdown menu-item-3082"><a title="Computer Components" href="https://demo2.chethemes.com/electro/product-category/computer-components/">Computer Components</a></li>
	                    <li id="menu-item-3083" class="menu-item menu-item-type-taxonomy menu-item-object-product_cat animate-dropdown menu-item-3083"><a title="Car Electronic &amp; GPS" href="https://demo2.chethemes.com/electro/product-category/car-electronic-gps/">Car Electronic &amp; GPS</a></li>
	                    <li id="menu-item-3084" class="menu-item menu-item-type-taxonomy menu-item-object-product_cat animate-dropdown menu-item-3084"><a title="Accessories" href="https://demo2.chethemes.com/electro/product-category/laptops-computers/accessories-laptops-computers/">Accessories</a></li>
	                    <li id="menu-item-3085" class="menu-item menu-item-type-taxonomy menu-item-object-product_cat animate-dropdown menu-item-3085"><a title="Printers &amp; Ink" href="https://demo2.chethemes.com/electro/product-category/printers-ink/">Printers &amp; Ink</a></li>
	                </ul>
	            </li>
	        </ul>
	        <form class="navbar-search" method="get" action="https://demo2.chethemes.com/electro/">
	            <label class="sr-only screen-reader-text" for="search">Search for:</label>
	            <div class="input-group">
	                <div class="input-search-field">
	                    <span class="twitter-typeahead" style="position: relative; display: table-cell;"><input type="text" id="search" class="form-control search-field product-search-field tt-input" dir="ltr" value="" name="s" placeholder="Search for Products" autocomplete="off" spellcheck="false" style="position: relative; vertical-align: top;"><pre aria-hidden="true" style="position: absolute; visibility: hidden; white-space: pre; font-family: &quot;Open Sans&quot;, HelveticaNeue-Light, &quot;Helvetica Neue Light&quot;, &quot;Helvetica Neue&quot;, Helvetica, Arial, &quot;Lucida Grande&quot;, sans-serif; font-size: 14.994px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; word-spacing: 0px; letter-spacing: 0px; text-indent: 0px; text-rendering: optimizeLegibility; text-transform: none;"></pre><div class="tt-menu" style="position: absolute; top: 100%; left: 0px; z-index: 100; display: none;"><div class="tt-dataset tt-dataset-search"></div></div></span>
	                </div>
	                <div class="input-group-addon search-categories">
	                    <select name="product_cat" id="product_cat" class="postform resizeselect" style="width: 143px;">
	                        <option value="0" selected="selected">All Categories</option>
	                        <option class="level-0" value="home-entertainment">Home Entertainment</option>
	                        <option class="level-0" value="laptops-computers">Laptops &amp; Computers</option>
	                        <option class="level-0" value="cameras-photography">Cameras &amp; Photography</option>
	                        <option class="level-0" value="smart-phones-tablets">Smart Phones &amp; Tablets</option>
	                        <option class="level-0" value="video-games-consoles">Video Games &amp; Consoles</option>
	                        <option class="level-0" value="tv-audio">TV &amp; Audio</option>
	                        <option class="level-0" value="gadgets">Gadgets</option>
	                        <option class="level-0" value="printers-ink">Printers &amp; Ink</option>
	                        <option class="level-0" value="computer-components">Computer Components</option>
	                        <option class="level-0" value="accessories">Accessories</option>
	                    </select>
	                </div>
	                <div class="input-group-btn">
	                    <input type="hidden" id="search-param" name="post_type" value="product">
	                    <button type="submit" class="btn btn-secondary"><i class="ec ec-search"></i></button>
	                </div>
	            </div>
	        </form>
	        <ul class="navbar-mini-cart navbar-nav animate-dropdown nav pull-right flip">
	            <li class="nav-item dropdown">
	                <a href="https://demo2.chethemes.com/electro/cart/" class="nav-link" data-toggle="dropdown">
	            <i class="ec ec-shopping-bag"></i>
	            <span class="cart-items-count count">1</span>
	            <span class="cart-items-total-price total-price"><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">$</span>1,999.00</span></span>
	        </a>
	                <ul class="dropdown-menu dropdown-menu-mini-cart">
	                    <li>
	                        <div class="widget_shopping_cart_content">
	                            <ul class="woocommerce-mini-cart cart_list product_list_widget ">
	                                <li class="woocommerce-mini-cart-item mini_cart_item">
	                                    <a href="https://demo2.chethemes.com/electro/cart/?remove_item=058d6f2fbe951a5a56d96b1f1a6bca1c&amp;_wpnonce=c63de743d6" class="remove" aria-label="Remove this item" data-product_id="2717" data-product_sku="5487FB8/41">×</a> <a href="https://demo2.chethemes.com/electro/product/tablet-red-elitebook-revolve-810-g2/">
	                                <img width="180" height="180" src="//demo2.chethemes.com/electro/wp-content/uploads/2016/03/Ultrabooks-180x180.jpg" class="attachment-shop_thumbnail size-shop_thumbnail wp-post-image" alt="" srcset="//demo2.chethemes.com/electro/wp-content/uploads/2016/03/Ultrabooks-180x180.jpg 180w, //demo2.chethemes.com/electro/wp-content/uploads/2016/03/Ultrabooks-150x150.jpg 150w, //demo2.chethemes.com/electro/wp-content/uploads/2016/03/Ultrabooks-300x300.jpg 300w, //demo2.chethemes.com/electro/wp-content/uploads/2016/03/Ultrabooks-600x600.jpg 600w" sizes="(max-width: 180px) 100vw, 180px">Tablet Red EliteBook  Revolve 810 G2&nbsp;                           </a>
	                                    <span class="quantity">1 × <span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">$</span>1,999.00</span>
	                                    </span>
	                                </li>
	                            </ul>
	                            <p class="woocommerce-mini-cart__total total"><strong>Subtotal:</strong> <span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">$</span>1,999.00</span>
	                            </p>
	                            <p class="woocommerce-mini-cart__buttons buttons"><a href="https://demo2.chethemes.com/electro/cart/" class="button wc-forward">View cart</a><a href="https://demo2.chethemes.com/electro/checkout/" class="button checkout wc-forward">Checkout</a></p>
	                        </div>
	                    </li>
	                </ul>
	            </li>
	        </ul>
	        <ul class="navbar-wishlist nav navbar-nav pull-right flip">
	            <li class="nav-item">
	                <a href="{{ route('wishlist') }}" class="nav-link">
	                        <i class="ec ec-favorites"></i>
	                                            </a>
	            </li>
	        </ul>
	        <ul class="navbar-compare nav navbar-nav pull-right flip">
	            <li class="nav-item">
	                <a href="https://demo2.chethemes.com/electro/compare/" class="nav-link">
	                        <i class="ec ec-compare"></i>
	                                            </a>
	            </li>
	        </ul>
	    </div>
	</nav>