<header id="masthead" class="site-header">
		<div class="container hidden-md-down">
					<div class="row">
					<div class="header-logo">
				<a href="/" class="header-logo-link">
					<svg version="1.1" x="0px" y="0px" width="175.748px" height="42.52px" viewBox="0 0 175.748 42.52" enable-background="new 0 0 175.748 42.52">
<ellipse class="ellipse-bg" fill-rule="evenodd" clip-rule="evenodd" fill="#FDD700" cx="170.05" cy="36.341" rx="5.32" ry="5.367"></ellipse>
<path fill-rule="evenodd" clip-rule="evenodd" fill="#333E48" d="M30.514,0.71c-0.034,0.003-0.066,0.008-0.056,0.056
	C30.263,0.995,29.876,1.181,29.79,1.5c-0.148,0.548,0,1.568,0,2.427v36.459c0.265,0.221,0.506,0.465,0.725,0.734h6.187
	c0.2-0.25,0.423-0.477,0.669-0.678V1.387C37.124,1.185,36.9,0.959,36.701,0.71H30.514z M117.517,12.731
	c-0.232-0.189-0.439-0.64-0.781-0.734c-0.754-0.209-2.039,0-3.121,0h-3.176V4.435c-0.232-0.189-0.439-0.639-0.781-0.733
	c-0.719-0.2-1.969,0-3.01,0h-3.01c-0.238,0.273-0.625,0.431-0.725,0.847c-0.203,0.852,0,2.399,0,3.725
	c0,1.393,0.045,2.748-0.055,3.725h-6.41c-0.184,0.237-0.629,0.434-0.725,0.791c-0.178,0.654,0,1.813,0,2.765v2.766
	c0.232,0.188,0.439,0.64,0.779,0.733c0.777,0.216,2.109,0,3.234,0c1.154,0,2.291-0.045,3.176,0.057v21.277
	c0.232,0.189,0.439,0.639,0.781,0.734c0.719,0.199,1.969,0,3.01,0h3.01c1.008-0.451,0.725-1.889,0.725-3.443
	c-0.002-6.164-0.047-12.867,0.055-18.625h6.299c0.182-0.236,0.627-0.434,0.725-0.79c0.176-0.653,0-1.813,0-2.765V12.731z
	 M135.851,18.262c0.201-0.746,0-2.029,0-3.104v-3.104c-0.287-0.245-0.434-0.637-0.781-0.733c-0.824-0.229-1.992-0.044-2.898,0
	c-2.158,0.104-4.506,0.675-5.74,1.411c-0.146-0.362-0.451-0.853-0.893-0.96c-0.693-0.169-1.859,0-2.842,0h-2.842
	c-0.258,0.319-0.625,0.42-0.725,0.79c-0.223,0.82,0,2.338,0,3.443c0,8.109-0.002,16.635,0,24.381
	c0.232,0.189,0.439,0.639,0.779,0.734c0.707,0.195,1.93,0,2.955,0h3.01c0.918-0.463,0.725-1.352,0.725-2.822V36.21
	c-0.002-3.902-0.242-9.117,0-12.473c0.297-4.142,3.836-4.877,8.527-4.686C135.312,18.816,135.757,18.606,135.851,18.262z
	 M14.796,11.376c-5.472,0.262-9.443,3.178-11.76,7.056c-2.435,4.075-2.789,10.62-0.501,15.126c2.043,4.023,5.91,7.115,10.701,7.9
	c6.051,0.992,10.992-1.219,14.324-3.838c-0.687-1.1-1.419-2.664-2.118-3.951c-0.398-0.734-0.652-1.486-1.616-1.467
	c-1.942,0.787-4.272,2.262-7.134,2.145c-3.791-0.154-6.659-1.842-7.524-4.91h19.452c0.146-2.793,0.22-5.338-0.279-7.563
	C26.961,15.728,22.503,11.008,14.796,11.376z M9,23.284c0.921-2.508,3.033-4.514,6.298-4.627c3.083-0.107,4.994,1.976,5.685,4.627
	C17.119,23.38,12.865,23.38,9,23.284z M52.418,11.376c-5.551,0.266-9.395,3.142-11.76,7.056
	c-2.476,4.097-2.829,10.493-0.557,15.069c1.997,4.021,5.895,7.156,10.646,7.957c6.068,1.023,11-1.227,14.379-3.781
	c-0.479-0.896-0.875-1.742-1.393-2.709c-0.312-0.582-1.024-2.234-1.561-2.539c-0.912-0.52-1.428,0.135-2.23,0.508
	c-0.564,0.262-1.223,0.523-1.672,0.676c-4.768,1.621-10.372,0.268-11.537-4.176h19.451c0.668-5.443-0.419-9.953-2.73-13.037
	C61.197,13.388,57.774,11.12,52.418,11.376z M46.622,23.343c0.708-2.553,3.161-4.578,6.242-4.686
	c3.08-0.107,5.08,1.953,5.686,4.686H46.622z M160.371,15.497c-2.455-2.453-6.143-4.291-10.869-4.064
	c-2.268,0.109-4.297,0.65-6.02,1.524c-1.719,0.873-3.092,1.957-4.234,3.217c-2.287,2.519-4.164,6.004-3.902,11.007
	c0.248,4.736,1.979,7.813,4.627,10.326c2.568,2.439,6.148,4.254,10.867,4.064c4.457-0.18,7.889-2.115,10.199-4.684
	c2.469-2.746,4.012-5.971,3.959-11.063C164.949,21.134,162.732,17.854,160.371,15.497z M149.558,33.952
	c-3.246-0.221-5.701-2.615-6.41-5.418c-0.174-0.689-0.26-1.25-0.4-2.166c-0.035-0.234,0.072-0.523-0.045-0.77
	c0.682-3.698,2.912-6.257,6.799-6.547c2.543-0.189,4.258,0.735,5.52,1.863c1.322,1.182,2.303,2.715,2.451,4.967
	C157.789,30.669,154.185,34.267,149.558,33.952z M88.812,29.55c-1.232,2.363-2.9,4.307-6.13,4.402
	c-4.729,0.141-8.038-3.16-8.025-7.563c0.004-1.412,0.324-2.65,0.947-3.726c1.197-2.061,3.507-3.688,6.633-3.612
	c3.222,0.079,4.966,1.708,6.632,3.668c1.328-1.059,2.529-1.948,3.9-2.99c0.416-0.315,1.076-0.688,1.227-1.072
	c0.404-1.031-0.365-1.502-0.891-2.088c-2.543-2.835-6.66-5.377-11.704-5.137c-6.02,0.288-10.218,3.697-12.484,7.846
	c-1.293,2.365-1.951,5.158-1.729,8.408c0.209,3.053,1.191,5.496,2.619,7.508c2.842,4.004,7.385,6.973,13.656,6.377
	c5.976-0.568,9.574-3.936,11.816-8.354c-0.141-0.271-0.221-0.604-0.336-0.902C92.929,31.364,90.843,30.485,88.812,29.55z"></path>
</svg>				</a>
			</div>
					<div class="primary-nav animate-dropdown">
			<div class="clearfix">
				 <button class="navbar-toggler hidden-sm-up pull-right flip" type="button" data-toggle="collapse" data-target="#default-header">
				    	☰
				 </button>
			 </div>

			<div class="collapse navbar-toggleable-xs" id="default-header">
				<ul id="menu-main-menu" class="nav nav-inline yamm"><li id="menu-item-3158" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children animate-dropdown menu-item-3158 dropdown"><a title="Home" href="https://demo2.chethemes.com/electro/shop/" data-toggle="dropdown" class="dropdown-toggle" aria-haspopup="true">Home</a>
<ul role="menu" class=" dropdown-menu">
	<li id="menu-item-3180" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home animate-dropdown menu-item-3180"><a title="Home v1" href="https://demo2.chethemes.com/electro/">Home v1</a></li>
	<li id="menu-item-3161" class="menu-item menu-item-type-post_type menu-item-object-page animate-dropdown menu-item-3161"><a title="Home v2" href="https://demo2.chethemes.com/electro/home-v2/">Home v2</a></li>
	<li id="menu-item-3160" class="menu-item menu-item-type-post_type menu-item-object-page animate-dropdown menu-item-3160"><a title="Home v3" href="https://demo2.chethemes.com/electro/home-v3/">Home v3</a></li>
</ul>
</li>
<li id="menu-item-3181" class="menu-item menu-item-type-post_type menu-item-object-page animate-dropdown menu-item-3181"><a title="About Us" href="https://demo2.chethemes.com/electro/about/">About Us</a></li>
<li id="menu-item-3159" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children animate-dropdown menu-item-3159 dropdown"><a title="Blog" href="https://demo2.chethemes.com/electro/blog/" data-toggle="dropdown" class="dropdown-toggle" aria-haspopup="true">Blog</a>
<ul role="menu" class=" dropdown-menu">
	<li id="menu-item-3164" class="menu-item menu-item-type-post_type menu-item-object-page animate-dropdown menu-item-3164"><a title="Blog v1" href="https://demo2.chethemes.com/electro/blog-v1/">Blog v1</a></li>
	<li id="menu-item-3163" class="menu-item menu-item-type-post_type menu-item-object-page animate-dropdown menu-item-3163"><a title="Blog v2" href="https://demo2.chethemes.com/electro/blog-v2/">Blog v2</a></li>
	<li id="menu-item-3165" class="menu-item menu-item-type-post_type menu-item-object-page animate-dropdown menu-item-3165"><a title="Blog v3" href="https://demo2.chethemes.com/electro/blog-v3/">Blog v3</a></li>
</ul>
</li>
<li id="menu-item-3062" class="yamm-fw menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children animate-dropdown menu-item-3062 dropdown"><a title="Pages" href="#" data-toggle="dropdown" class="dropdown-toggle" aria-haspopup="true">Pages</a>
<ul role="menu" class=" dropdown-menu">
	<li id="menu-item-3185" class="menu-item menu-item-type-post_type menu-item-object-static_block animate-dropdown menu-item-3185"><div class="yamm-content"><div class="vc_row wpb_row vc_row-fluid"><div class="wpb_column vc_column_container vc_col-sm-3"><div class="vc_column-inner "><div class="wpb_wrapper"><div class="vc_wp_custommenu wpb_content_element"><div class="widget widget_nav_menu"><div class="menu-pages-menu-1-container"><ul id="menu-pages-menu-1" class="menu"><li id="menu-item-3120" class="nav-title menu-item menu-item-type-custom menu-item-object-custom menu-item-3120"><a href="#">Home &amp; Static Pages</a></li>
<li id="menu-item-3184" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home menu-item-3184"><a href="https://demo2.chethemes.com/electro/">Home v1</a></li>
<li id="menu-item-3182" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3182"><a href="https://demo2.chethemes.com/electro/home-v2/">Home v2</a></li>
<li id="menu-item-3183" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3183"><a href="https://demo2.chethemes.com/electro/home-v3/">Home v3</a></li>
<li id="menu-item-3189" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3189"><a href="https://demo2.chethemes.com/electro/about/">About</a></li>
<li id="menu-item-3186" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3186"><a href="https://demo2.chethemes.com/electro/contact-v2/">Contact v2</a></li>
<li id="menu-item-3187" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3187"><a href="https://demo2.chethemes.com/electro/contact-v1/">Contact v1</a></li>
<li id="menu-item-3188" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3188"><a href="https://demo2.chethemes.com/electro/faq/">FAQ</a></li>
<li id="menu-item-3204" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3204"><a href="https://demo2.chethemes.com/electro/store-directory/">Store Directory</a></li>
<li id="menu-item-3205" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3205"><a href="https://demo2.chethemes.com/electro/terms-and-conditions/">Terms and Conditions</a></li>
<li id="menu-item-3142" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-3142"><a href="https://demo2.chethemes.com/electro/?page_id=12343434">404</a></li>
</ul></div></div></div></div></div></div><div class="wpb_column vc_column_container vc_col-sm-3"><div class="vc_column-inner "><div class="wpb_wrapper"><div class="vc_wp_custommenu wpb_content_element"><div class="widget widget_nav_menu"><div class="menu-pages-menu-2-container"><ul id="menu-pages-menu-2" class="menu"><li id="menu-item-3126" class="nav-title menu-item menu-item-type-custom menu-item-object-custom menu-item-3126"><a href="#">Shop Pages</a></li>
<li id="menu-item-3127" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-3127"><a href="https://demo2.chethemes.com/electro/product-category/smart-phones-tablets/#grid">Shop Grid</a></li>
<li id="menu-item-3128" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-3128"><a href="https://demo2.chethemes.com/electro/product-category/smart-phones-tablets/#grid-extended">Shop Grid Extended</a></li>
<li id="menu-item-3129" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-3129"><a href="https://demo2.chethemes.com/electro/product-category/smart-phones-tablets/#list-view">Shop List View</a></li>
<li id="menu-item-3130" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-3130"><a href="https://demo2.chethemes.com/electro/product-category/smart-phones-tablets/#list-view-small">Shop List View Small</a></li>
<li id="menu-item-3145" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-3145"><a href="https://demo2.chethemes.com/electro/?page_id=3030&amp;shop_columns=3&amp;shop_layout=left-sidebar">Shop Left Sidebar</a></li>
<li id="menu-item-3143" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-3143"><a href="https://demo2.chethemes.com/electro/?page_id=3030&amp;shop_columns=5&amp;shop_layout=full-width">Shop Full width</a></li>
<li id="menu-item-3144" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-3144"><a href="https://demo2.chethemes.com/electro/?page_id=3030&amp;shop_columns=3&amp;shop_layout=right-sidebar">Shop Right Sidebar</a></li>
<li id="menu-item-3121" class="nav-title menu-item menu-item-type-custom menu-item-object-custom menu-item-3121"><a href="#">Product Categories</a></li>
<li id="menu-item-3123" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-3123"><a href="https://demo2.chethemes.com/electro/?product_cat=laptops-computers&amp;cat_columns=3&amp;cat_layout=left-sidebar">3 Column Sidebar</a></li>
<li id="menu-item-3122" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-3122"><a href="https://demo2.chethemes.com/electro/?product_cat=laptops-computers&amp;cat_columns=4&amp;cat_layout=left-sidebar">4 Column Sidebar</a></li>
<li id="menu-item-3124" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-3124"><a href="https://demo2.chethemes.com/electro/?product_cat=laptops-computers&amp;cat_columns=4&amp;cat_layout=full-width">4 Column Full width</a></li>
<li id="menu-item-3125" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-3125"><a href="https://demo2.chethemes.com/electro/?product_cat=laptops-computers&amp;cat_columns=6&amp;cat_layout=full-width">6 Columns Full width</a></li>
</ul></div></div></div></div></div></div><div class="wpb_column vc_column_container vc_col-sm-3"><div class="vc_column-inner "><div class="wpb_wrapper"><div class="vc_wp_custommenu wpb_content_element"><div class="widget widget_nav_menu"><div class="menu-pages-menu-3-container"><ul id="menu-pages-menu-3" class="menu"><li id="menu-item-3133" class="nav-title menu-item menu-item-type-custom menu-item-object-custom menu-item-3133"><a href="#">Single Product Pages</a></li>
<li id="menu-item-3203" class="menu-item menu-item-type-post_type menu-item-object-product menu-item-3203"><a href="https://demo2.chethemes.com/electro/product/ultra-wireless-s50-headphones-s50-with-bluetooth/">Single Product Extended</a></li>
<li id="menu-item-3202" class="menu-item menu-item-type-post_type menu-item-object-product menu-item-3202"><a href="https://demo2.chethemes.com/electro/product/ultra-wireless-s50-headphones-s50-with-bluetooth-fullwidth/">Single Product Fullwidth</a></li>
<li id="menu-item-3201" class="menu-item menu-item-type-post_type menu-item-object-product menu-item-3201"><a href="https://demo2.chethemes.com/electro/product/ultra-wireless-s50-headphones-s50-with-bluetooth-sidebar/">Single Product Sidebar</a></li>
<li id="menu-item-3132" class="nav-title menu-item menu-item-type-custom menu-item-object-custom menu-item-3132"><a href="#">WooCommerce Pages</a></li>
<li id="menu-item-3193" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3193"><a href="https://demo2.chethemes.com/electro/shop/">Shop</a></li>
<li id="menu-item-3190" class="menu-item menu-item-type-post_type menu-item-object-page current-menu-item page_item page-item-3134 current_page_item menu-item-3190"><a href="https://demo2.chethemes.com/electro/cart/">Cart</a></li>
<li id="menu-item-3191" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3191"><a href="https://demo2.chethemes.com/electro/checkout/">Checkout</a></li>
<li id="menu-item-3192" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3192"><a href="https://demo2.chethemes.com/electro/my-account/">My Account</a></li>
<li id="menu-item-3194" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3194"><a href="https://demo2.chethemes.com/electro/track-your-order/">Track your Order</a></li>
<li id="menu-item-3224" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3224"><a href="https://demo2.chethemes.com/electro/compare/">Compare</a></li>
</ul></div></div></div></div></div></div><div class="wpb_column vc_column_container vc_col-sm-3"><div class="vc_column-inner "><div class="wpb_wrapper"><div class="vc_wp_custommenu wpb_content_element"><div class="widget widget_nav_menu"><div class="menu-pages-menu-4-container"><ul id="menu-pages-menu-4" class="menu"><li id="menu-item-3131" class="nav-title menu-item menu-item-type-custom menu-item-object-custom menu-item-3131"><a href="#">Blog Pages</a></li>
<li id="menu-item-3196" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3196"><a href="https://demo2.chethemes.com/electro/blog-v1/">Blog v1</a></li>
<li id="menu-item-3197" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3197"><a href="https://demo2.chethemes.com/electro/blog-v3/">Blog v3</a></li>
<li id="menu-item-3198" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3198"><a href="https://demo2.chethemes.com/electro/blog-v2/">Blog v2</a></li>
<li id="menu-item-3199" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3199"><a href="https://demo2.chethemes.com/electro/blog-full-width/">Blog Full Width</a></li>
<li id="menu-item-3200" class="menu-item menu-item-type-post_type menu-item-object-post menu-item-3200"><a href="https://demo2.chethemes.com/electro/2016/03/01/robot-wars-now-closed/">Single Blog Post</a></li>
<li id="menu-item-3141" class="nav-title menu-item menu-item-type-post_type menu-item-object-page menu-item-3141"><a href="https://demo2.chethemes.com/electro/shop/">Shop Columns</a></li>
<li id="menu-item-3136" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-3136"><a href="https://demo2.chethemes.com/electro/?page_id=3030&amp;shop_columns=6&amp;shop_layout=full-width">6 Columns Full width</a></li>
<li id="menu-item-3137" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-3137"><a href="https://demo2.chethemes.com/electro/?page_id=3030&amp;shop_columns=5&amp;shop_layout=full-width">5 Columns Full width</a></li>
<li id="menu-item-3138" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-3138"><a href="https://demo2.chethemes.com/electro/?page_id=3030&amp;shop_columns=4&amp;shop_layout=left-sidebar">4 Columns Sidebar</a></li>
<li id="menu-item-3139" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-3139"><a href="https://demo2.chethemes.com/electro/?page_id=3030&amp;shop_columns=3&amp;shop_layout=left-sidebar">3 Columns Sidebar</a></li>
<li id="menu-item-3140" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-3140"><a href="https://demo2.chethemes.com/electro/?page_id=3030&amp;shop_columns=2&amp;shop_layout=left-sidebar">2 Columns Sidebar</a></li>
</ul></div></div></div></div></div></div></div>
</div></li>
</ul>
</li>
<li id="menu-item-3063" class="menu-item menu-item-type-custom menu-item-object-custom animate-dropdown menu-item-3063"><a title="Features" href="#">Features</a></li>
<li id="menu-item-3064" class="menu-item menu-item-type-custom menu-item-object-custom animate-dropdown menu-item-3064"><a title="Contact Us" href="#">Contact Us</a></li>
</ul>			</div>
		</div>
				<div class="header-support-info">
			<div class="media">
				<span class="media-left support-icon media-middle"><i class="ec ec-support"></i></span>
				<div class="media-body">
					<span class="support-number"><strong>Support</strong> (+800) 856 800 604</span><br>
					<span class="support-email">Email: info@electro.com</span>
				</div>
			</div>
		</div>		</div><!-- /.row -->

		</div>

					<div class="container hidden-lg-up">
				<div class="handheld-header">
								<div class="header-logo">
				<a href="https://demo2.chethemes.com/electro/" class="header-logo-link">
					<svg version="1.1" x="0px" y="0px" width="175.748px" height="42.52px" viewBox="0 0 175.748 42.52" enable-background="new 0 0 175.748 42.52">
<ellipse class="ellipse-bg" fill-rule="evenodd" clip-rule="evenodd" fill="#FDD700" cx="170.05" cy="36.341" rx="5.32" ry="5.367"></ellipse>
<path fill-rule="evenodd" clip-rule="evenodd" fill="#333E48" d="M30.514,0.71c-0.034,0.003-0.066,0.008-0.056,0.056
	C30.263,0.995,29.876,1.181,29.79,1.5c-0.148,0.548,0,1.568,0,2.427v36.459c0.265,0.221,0.506,0.465,0.725,0.734h6.187
	c0.2-0.25,0.423-0.477,0.669-0.678V1.387C37.124,1.185,36.9,0.959,36.701,0.71H30.514z M117.517,12.731
	c-0.232-0.189-0.439-0.64-0.781-0.734c-0.754-0.209-2.039,0-3.121,0h-3.176V4.435c-0.232-0.189-0.439-0.639-0.781-0.733
	c-0.719-0.2-1.969,0-3.01,0h-3.01c-0.238,0.273-0.625,0.431-0.725,0.847c-0.203,0.852,0,2.399,0,3.725
	c0,1.393,0.045,2.748-0.055,3.725h-6.41c-0.184,0.237-0.629,0.434-0.725,0.791c-0.178,0.654,0,1.813,0,2.765v2.766
	c0.232,0.188,0.439,0.64,0.779,0.733c0.777,0.216,2.109,0,3.234,0c1.154,0,2.291-0.045,3.176,0.057v21.277
	c0.232,0.189,0.439,0.639,0.781,0.734c0.719,0.199,1.969,0,3.01,0h3.01c1.008-0.451,0.725-1.889,0.725-3.443
	c-0.002-6.164-0.047-12.867,0.055-18.625h6.299c0.182-0.236,0.627-0.434,0.725-0.79c0.176-0.653,0-1.813,0-2.765V12.731z
	 M135.851,18.262c0.201-0.746,0-2.029,0-3.104v-3.104c-0.287-0.245-0.434-0.637-0.781-0.733c-0.824-0.229-1.992-0.044-2.898,0
	c-2.158,0.104-4.506,0.675-5.74,1.411c-0.146-0.362-0.451-0.853-0.893-0.96c-0.693-0.169-1.859,0-2.842,0h-2.842
	c-0.258,0.319-0.625,0.42-0.725,0.79c-0.223,0.82,0,2.338,0,3.443c0,8.109-0.002,16.635,0,24.381
	c0.232,0.189,0.439,0.639,0.779,0.734c0.707,0.195,1.93,0,2.955,0h3.01c0.918-0.463,0.725-1.352,0.725-2.822V36.21
	c-0.002-3.902-0.242-9.117,0-12.473c0.297-4.142,3.836-4.877,8.527-4.686C135.312,18.816,135.757,18.606,135.851,18.262z
	 M14.796,11.376c-5.472,0.262-9.443,3.178-11.76,7.056c-2.435,4.075-2.789,10.62-0.501,15.126c2.043,4.023,5.91,7.115,10.701,7.9
	c6.051,0.992,10.992-1.219,14.324-3.838c-0.687-1.1-1.419-2.664-2.118-3.951c-0.398-0.734-0.652-1.486-1.616-1.467
	c-1.942,0.787-4.272,2.262-7.134,2.145c-3.791-0.154-6.659-1.842-7.524-4.91h19.452c0.146-2.793,0.22-5.338-0.279-7.563
	C26.961,15.728,22.503,11.008,14.796,11.376z M9,23.284c0.921-2.508,3.033-4.514,6.298-4.627c3.083-0.107,4.994,1.976,5.685,4.627
	C17.119,23.38,12.865,23.38,9,23.284z M52.418,11.376c-5.551,0.266-9.395,3.142-11.76,7.056
	c-2.476,4.097-2.829,10.493-0.557,15.069c1.997,4.021,5.895,7.156,10.646,7.957c6.068,1.023,11-1.227,14.379-3.781
	c-0.479-0.896-0.875-1.742-1.393-2.709c-0.312-0.582-1.024-2.234-1.561-2.539c-0.912-0.52-1.428,0.135-2.23,0.508
	c-0.564,0.262-1.223,0.523-1.672,0.676c-4.768,1.621-10.372,0.268-11.537-4.176h19.451c0.668-5.443-0.419-9.953-2.73-13.037
	C61.197,13.388,57.774,11.12,52.418,11.376z M46.622,23.343c0.708-2.553,3.161-4.578,6.242-4.686
	c3.08-0.107,5.08,1.953,5.686,4.686H46.622z M160.371,15.497c-2.455-2.453-6.143-4.291-10.869-4.064
	c-2.268,0.109-4.297,0.65-6.02,1.524c-1.719,0.873-3.092,1.957-4.234,3.217c-2.287,2.519-4.164,6.004-3.902,11.007
	c0.248,4.736,1.979,7.813,4.627,10.326c2.568,2.439,6.148,4.254,10.867,4.064c4.457-0.18,7.889-2.115,10.199-4.684
	c2.469-2.746,4.012-5.971,3.959-11.063C164.949,21.134,162.732,17.854,160.371,15.497z M149.558,33.952
	c-3.246-0.221-5.701-2.615-6.41-5.418c-0.174-0.689-0.26-1.25-0.4-2.166c-0.035-0.234,0.072-0.523-0.045-0.77
	c0.682-3.698,2.912-6.257,6.799-6.547c2.543-0.189,4.258,0.735,5.52,1.863c1.322,1.182,2.303,2.715,2.451,4.967
	C157.789,30.669,154.185,34.267,149.558,33.952z M88.812,29.55c-1.232,2.363-2.9,4.307-6.13,4.402
	c-4.729,0.141-8.038-3.16-8.025-7.563c0.004-1.412,0.324-2.65,0.947-3.726c1.197-2.061,3.507-3.688,6.633-3.612
	c3.222,0.079,4.966,1.708,6.632,3.668c1.328-1.059,2.529-1.948,3.9-2.99c0.416-0.315,1.076-0.688,1.227-1.072
	c0.404-1.031-0.365-1.502-0.891-2.088c-2.543-2.835-6.66-5.377-11.704-5.137c-6.02,0.288-10.218,3.697-12.484,7.846
	c-1.293,2.365-1.951,5.158-1.729,8.408c0.209,3.053,1.191,5.496,2.619,7.508c2.842,4.004,7.385,6.973,13.656,6.377
	c5.976-0.568,9.574-3.936,11.816-8.354c-0.141-0.271-0.221-0.604-0.336-0.902C92.929,31.364,90.843,30.485,88.812,29.55z"></path>
</svg>				</a>
			</div>
					<div class="handheld-navigation-wrapper">
			<div class="handheld-navbar-toggle-buttons clearfix">
				<button id="button-open" class="navbar-toggler navbar-toggle-hamburger hidden-lg-up pull-right flip" type="button">
					<i class="fa fa-bars" aria-hidden="true"></i>
				</button>
				<button id="button-close" class="navbar-toggler navbar-toggle-close hidden-lg-up pull-right flip" type="button">
					<i class="ec ec-close-remove"></i>
				</button>
			</div>

			<div class="handheld-navigation hidden-lg-up" id="default-hh-header">
				<span class="ehm-close">Close</span>
				<ul id="menu-all-departments-menu" class="nav nav-inline yamm"><li id="menu-item-3214" class="highlight menu-item menu-item-type-post_type menu-item-object-page animate-dropdown menu-item-3214"><a title="Value of the Day" href="https://demo2.chethemes.com/electro/home-v2/">Value of the Day</a></li>
<li id="menu-item-3215" class="highlight menu-item menu-item-type-post_type menu-item-object-page animate-dropdown menu-item-3215"><a title="Top 100 Offers" href="https://demo2.chethemes.com/electro/home-v3/">Top 100 Offers</a></li>
<li id="menu-item-3216" class="highlight menu-item menu-item-type-post_type menu-item-object-page animate-dropdown menu-item-3216"><a title="New Arrivals" href="https://demo2.chethemes.com/electro/home-v3-full-color-background/">New Arrivals</a></li>
<li id="menu-item-3094" class="yamm-tfw menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-has-children animate-dropdown menu-item-3094 dropdown"><a title="Computers &amp; Accessories" href="https://demo2.chethemes.com/electro/product-category/laptops-computers/" data-toggle="dropdown" class="dropdown-toggle" aria-haspopup="true">Computers &amp; Accessories</a>
<ul role="menu" class=" dropdown-menu">
	<li id="menu-item-3174" class="menu-item menu-item-type-post_type menu-item-object-static_block animate-dropdown menu-item-3174"><div class="yamm-content"><div class="vc_row wpb_row vc_row-fluid bg-yamm-content bg-yamm-content-bottom bg-yamm-content-right"><div class="wpb_column vc_column_container vc_col-sm-12"><div class="vc_column-inner "><div class="wpb_wrapper">
	<div class="wpb_single_image wpb_content_element vc_align_left">

		<figure class="wpb_wrapper vc_figure">
			<div class="vc_single_image-wrapper   vc_box_border_grey"><img width="540" height="460" src="https://demo2.chethemes.com/electro/wp-content/uploads/2016/03/megamenu-2.png" class="vc_single_image-img attachment-full" alt="" srcset="https://demo2.chethemes.com/electro/wp-content/uploads/2016/03/megamenu-2.png 540w, https://demo2.chethemes.com/electro/wp-content/uploads/2016/03/megamenu-2-300x256.png 300w" sizes="(max-width: 540px) 100vw, 540px"></div>
		</figure>
	</div>
</div></div></div></div><div class="vc_row wpb_row vc_row-fluid"><div class="wpb_column vc_column_container vc_col-sm-6"><div class="vc_column-inner "><div class="wpb_wrapper">
	<div class="wpb_text_column wpb_content_element ">
		<div class="wpb_wrapper">
			<ul>
<li class="nav-title">Computers &amp; Accessories</li>
<li><a href="#">All Computers &amp; Accessories</a></li>
<li><a href="#">Laptops, Desktops &amp; Monitors</a></li>
<li><a href="#">Pen Drives, Hard Drives &amp; Memory Cards</a></li>
<li><a href="#">Printers &amp; Ink</a></li>
<li><a href="#">Networking &amp; Internet Devices</a></li>
<li><a href="#">Computer Accessories</a></li>
<li><a href="#">Software</a></li>
<li class="nav-divider"></li>
<li><a href="#"><span class="nav-text">All Electronics</span><span class="nav-subtext">Discover more products</span></a></li>
</ul>

		</div>
	</div>
</div></div></div><div class="wpb_column vc_column_container vc_col-sm-6"><div class="vc_column-inner "><div class="wpb_wrapper">
	<div class="wpb_text_column wpb_content_element ">
		<div class="wpb_wrapper">
			<ul>
<li class="nav-title">Office &amp; Stationery</li>
<li><a href="#">All Office &amp; Stationery</a></li>
<li><a href="#">Pens &amp; Writing</a></li>
</ul>

		</div>
	</div>
</div></div></div></div>
</div></li>
</ul>
</li>
<li id="menu-item-3095" class="yamm-tfw menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-has-children animate-dropdown menu-item-3095 dropdown"><a title="Cameras, Audio &amp; Video" href="https://demo2.chethemes.com/electro/product-category/cameras-photography/" data-toggle="dropdown" class="dropdown-toggle" aria-haspopup="true">Cameras, Audio &amp; Video</a>
<ul role="menu" class=" dropdown-menu">
	<li id="menu-item-3173" class="menu-item menu-item-type-post_type menu-item-object-static_block animate-dropdown menu-item-3173"><div class="yamm-content"><div class="vc_row wpb_row vc_row-fluid bg-yamm-content"><div class="wpb_column vc_column_container vc_col-sm-12"><div class="vc_column-inner "><div class="wpb_wrapper">
	<div class="wpb_single_image wpb_content_element vc_align_left">

		<figure class="wpb_wrapper vc_figure">
			<div class="vc_single_image-wrapper   vc_box_border_grey"><img width="540" height="460" src="https://demo2.chethemes.com/electro/wp-content/uploads/2016/03/megamenu-3.png" class="vc_single_image-img attachment-full" alt="" srcset="https://demo2.chethemes.com/electro/wp-content/uploads/2016/03/megamenu-3.png 540w, https://demo2.chethemes.com/electro/wp-content/uploads/2016/03/megamenu-3-300x256.png 300w" sizes="(max-width: 540px) 100vw, 540px"></div>
		</figure>
	</div>
</div></div></div></div><div class="vc_row wpb_row vc_row-fluid"><div class="wpb_column vc_column_container vc_col-sm-6"><div class="vc_column-inner "><div class="wpb_wrapper">
	<div class="wpb_text_column wpb_content_element ">
		<div class="wpb_wrapper">
			<ul>
<li class="nav-title"><a href="#">Cameras &amp; Photography</a></li>
<li><a href="#">All Cameras &amp; Photography</a></li>
<li><a href="#">Digital SLRs</a></li>
<li><a href="#">Point &amp; Shoot Cameras</a></li>
<li><a href="#">Lenses</a></li>
<li><a href="#">Camera Accessories</a></li>
<li><a href="#">Security &amp; Surveillance</a></li>
<li><a href="#">Binoculars &amp; Telescopes</a></li>
<li><a href="#">Camcorders</a></li>
<li class="nav-divider"></li>
<li><a href="#"><span class="nav-text">All Electronics</span><span class="nav-subtext">Discover more products</span></a></li>
</ul>

		</div>
	</div>
</div></div></div><div class="wpb_column vc_column_container vc_col-sm-6"><div class="vc_column-inner "><div class="wpb_wrapper">
	<div class="wpb_text_column wpb_content_element ">
		<div class="wpb_wrapper">
			<ul>
<li class="nav-title">Audio &amp; Video</li>
<li><a href="#">All Audio &amp; Video</a></li>
<li><a href="#">Headphones &amp; Speakers</a></li>
<li><a href="#">Home Entertainment Systems</a></li>
<li><a href="#">MP3 &amp; Media Players</a></li>
</ul>

		</div>
	</div>
</div></div></div></div>
</div></li>
</ul>
</li>
<li id="menu-item-3096" class="yamm-tfw menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-has-children animate-dropdown menu-item-3096 dropdown"><a title="Mobiles &amp; Tablets" href="https://demo2.chethemes.com/electro/product-category/smart-phones-tablets/" data-toggle="dropdown" class="dropdown-toggle" aria-haspopup="true">Mobiles &amp; Tablets</a>
<ul role="menu" class=" dropdown-menu">
	<li id="menu-item-3175" class="menu-item menu-item-type-post_type menu-item-object-static_block animate-dropdown menu-item-3175"><div class="yamm-content"><div class="vc_row wpb_row vc_row-fluid bg-yamm-content"><div class="wpb_column vc_column_container vc_col-sm-12"><div class="vc_column-inner "><div class="wpb_wrapper">
	<div class="wpb_single_image wpb_content_element vc_align_left   bg-yamm-extend-outside">

		<figure class="wpb_wrapper vc_figure">
			<div class="vc_single_image-wrapper   vc_box_border_grey"><img width="540" height="495" src="https://demo2.chethemes.com/electro/wp-content/uploads/2016/03/megamenu-.png" class="vc_single_image-img attachment-full" alt="" srcset="https://demo2.chethemes.com/electro/wp-content/uploads/2016/03/megamenu-.png 540w, https://demo2.chethemes.com/electro/wp-content/uploads/2016/03/megamenu--300x275.png 300w" sizes="(max-width: 540px) 100vw, 540px"></div>
		</figure>
	</div>
</div></div></div></div><div class="vc_row wpb_row vc_row-fluid"><div class="wpb_column vc_column_container vc_col-sm-6"><div class="vc_column-inner "><div class="wpb_wrapper">
	<div class="wpb_text_column wpb_content_element ">
		<div class="wpb_wrapper">
			<ul>
<li class="nav-title">Mobiles &amp; Tablets</li>
<li><a href="#">All Mobile Phones</a></li>
<li><a href="#">Smartphones</a></li>
<li><a href="#">Android Mobiles</a></li>
<li><a href="#">Windows Mobiles</a></li>
<li><a href="#">Refurbished Mobiles</a></li>
<li class="nav-divider"></li>
<li><a href="#">All Mobile Accessories</a></li>
<li><a href="#">Cases &amp; Covers</a></li>
<li><a href="#">Screen Protectors</a></li>
<li><a href="#">Power Banks</a></li>
<li class="nav-divider"></li>
<li><a href="#"><span class="nav-text">All Electronics</span><span class="nav-subtext">Discover more products</span></a></li>
</ul>

		</div>
	</div>
</div></div></div><div class="wpb_column vc_column_container vc_col-sm-6"><div class="vc_column-inner "><div class="wpb_wrapper">
	<div class="wpb_text_column wpb_content_element ">
		<div class="wpb_wrapper">
			<ul>
<li class="nav-title"></li>
<li><a href="#">All Tablets</a></li>
<li><a href="#">Tablet Accessories</a></li>
<li><a href="#">Landline Phones</a></li>
<li><a href="#">Wearable Devices</a></li>
</ul>

		</div>
	</div>
</div></div></div></div>
</div></li>
</ul>
</li>
<li id="menu-item-3097" class="yamm-tfw menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-has-children animate-dropdown menu-item-3097 dropdown"><a title="Movies, Music &amp; Video Games" href="https://demo2.chethemes.com/electro/product-category/video-games-consoles/" data-toggle="dropdown" class="dropdown-toggle" aria-haspopup="true">Movies, Music &amp; Video Games</a>
<ul role="menu" class=" dropdown-menu">
	<li id="menu-item-3176" class="menu-item menu-item-type-post_type menu-item-object-static_block animate-dropdown menu-item-3176"><div class="yamm-content"><div class="vc_row wpb_row vc_row-fluid bg-yamm-content"><div class="wpb_column vc_column_container vc_col-sm-12"><div class="vc_column-inner "><div class="wpb_wrapper">
	<div class="wpb_single_image wpb_content_element vc_align_left">

		<figure class="wpb_wrapper vc_figure">
			<div class="vc_single_image-wrapper   vc_box_border_grey"><img width="540" height="485" src="https://demo2.chethemes.com/electro/wp-content/uploads/2016/03/megamenu-8.png" class="vc_single_image-img attachment-full" alt="" srcset="https://demo2.chethemes.com/electro/wp-content/uploads/2016/03/megamenu-8.png 540w, https://demo2.chethemes.com/electro/wp-content/uploads/2016/03/megamenu-8-300x269.png 300w" sizes="(max-width: 540px) 100vw, 540px"></div>
		</figure>
	</div>
</div></div></div></div><div class="vc_row wpb_row vc_row-fluid"><div class="wpb_column vc_column_container vc_col-sm-6"><div class="vc_column-inner "><div class="wpb_wrapper">
	<div class="wpb_text_column wpb_content_element ">
		<div class="wpb_wrapper">
			<ul>
<li class="nav-title">Movies &amp; TV Shows</li>
<li><a href="#">All Movies &amp; TV Shows</a></li>
<li><a href="#">Blu-ray</a></li>
<li><a href="#">All English</a></li>
<li><a href="#">All Hindi</a></li>
<li class="nav-divider"></li>
<li class="nav-title">Video Games</li>
<li><a href="#">All Consoles, Games &amp; Accessories</a></li>
<li><a href="#">PC Games</a></li>
<li><a href="#">Pre-orders &amp; New Releases</a></li>
<li><a href="#">Consoles</a></li>
<li><a href="#">Accessories</a></li>
</ul>

		</div>
	</div>
</div></div></div><div class="wpb_column vc_column_container vc_col-sm-6"><div class="vc_column-inner "><div class="wpb_wrapper">
	<div class="wpb_text_column wpb_content_element ">
		<div class="wpb_wrapper">
			<ul>
<li class="nav-title">Music</li>
<li><a href="#">All Music</a></li>
<li><a href="#">International Music</a></li>
<li><a href="#">Film Songs</a></li>
<li><a href="#">Indian Classical</a></li>
<li><a href="#">Musical Instruments</a></li>
</ul>

		</div>
	</div>
</div></div></div></div>
</div></li>
</ul>
</li>
<li id="menu-item-3098" class="yamm-tfw menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-has-children animate-dropdown menu-item-3098 dropdown"><a title="TV &amp; Audio" href="https://demo2.chethemes.com/electro/product-category/tv-audio/" data-toggle="dropdown" class="dropdown-toggle" aria-haspopup="true">TV &amp; Audio</a>
<ul role="menu" class=" dropdown-menu">
	<li id="menu-item-3178" class="menu-item menu-item-type-post_type menu-item-object-static_block animate-dropdown menu-item-3178"><div class="yamm-content"><div class="vc_row wpb_row vc_row-fluid bg-yamm-content"><div class="wpb_column vc_column_container vc_col-sm-12"><div class="vc_column-inner "><div class="wpb_wrapper">
	<div class="wpb_single_image wpb_content_element vc_align_left">

		<figure class="wpb_wrapper vc_figure">
			<div class="vc_single_image-wrapper   vc_box_border_grey"><img width="540" height="460" src="https://demo2.chethemes.com/electro/wp-content/uploads/2016/03/megamenu-4.png" class="vc_single_image-img attachment-full" alt="" srcset="https://demo2.chethemes.com/electro/wp-content/uploads/2016/03/megamenu-4.png 540w, https://demo2.chethemes.com/electro/wp-content/uploads/2016/03/megamenu-4-300x256.png 300w" sizes="(max-width: 540px) 100vw, 540px"></div>
		</figure>
	</div>
</div></div></div></div><div class="vc_row wpb_row vc_row-fluid"><div class="wpb_column vc_column_container vc_col-sm-6"><div class="vc_column-inner "><div class="wpb_wrapper">
	<div class="wpb_text_column wpb_content_element ">
		<div class="wpb_wrapper">
			<ul>
<li class="nav-title">Audio &amp; Video</li>
<li><a href="#">All Audio &amp; Video</a></li>
<li><a href="#">Televisions</a></li>
<li><a href="#">Headphones</a></li>
<li><a href="#">Speakers</a></li>
<li><a href="#">Home Entertainment Systems</a></li>
<li><a href="#">MP3 &amp; Media Players</a></li>
<li><a href="#">Audio &amp; Video Accessories</a></li>
<li class="nav-divider"></li>
<li><a href="#"><span class="nav-text">Electro Home Appliances</span><span class="nav-subtext">Available in select cities</span></a></li>
</ul>

		</div>
	</div>
</div></div></div><div class="wpb_column vc_column_container vc_col-sm-6"><div class="vc_column-inner "><div class="wpb_wrapper">
	<div class="wpb_text_column wpb_content_element ">
		<div class="wpb_wrapper">
			<ul>
<li class="nav-title">Music</li>
<li><a href="#">Televisions</a></li>
<li><a href="#">Headphones</a></li>
<li><a href="#">Speakers</a></li>
<li><a href="#">Media Players</a></li>
</ul>

		</div>
	</div>
</div></div></div></div>
</div></li>
</ul>
</li>
<li id="menu-item-3099" class="yamm-tfw menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-has-children animate-dropdown menu-item-3099 dropdown"><a title="Watches &amp; Eyewear" href="https://demo2.chethemes.com/electro/product-category/gadgets/" data-toggle="dropdown" class="dropdown-toggle" aria-haspopup="true">Watches &amp; Eyewear</a>
<ul role="menu" class=" dropdown-menu">
	<li id="menu-item-3177" class="menu-item menu-item-type-post_type menu-item-object-static_block animate-dropdown menu-item-3177"><div class="yamm-content"><div class="vc_row wpb_row vc_row-fluid bg-yamm-content"><div class="wpb_column vc_column_container vc_col-sm-12"><div class="vc_column-inner "><div class="wpb_wrapper">
	<div class="wpb_single_image wpb_content_element vc_align_left">

		<figure class="wpb_wrapper vc_figure">
			<div class="vc_single_image-wrapper   vc_box_border_grey"><img width="540" height="486" src="https://demo2.chethemes.com/electro/wp-content/uploads/2016/03/megamenu-7.png" class="vc_single_image-img attachment-full" alt="" srcset="https://demo2.chethemes.com/electro/wp-content/uploads/2016/03/megamenu-7.png 540w, https://demo2.chethemes.com/electro/wp-content/uploads/2016/03/megamenu-7-300x270.png 300w" sizes="(max-width: 540px) 100vw, 540px"></div>
		</figure>
	</div>
</div></div></div></div><div class="vc_row wpb_row vc_row-fluid"><div class="wpb_column vc_column_container vc_col-sm-6"><div class="vc_column-inner "><div class="wpb_wrapper">
	<div class="wpb_text_column wpb_content_element ">
		<div class="wpb_wrapper">
			<ul>
<li class="nav-title">Watches</li>
<li><a href="#">All Watches</a></li>
<li><a href="#">Men’s Watches</a></li>
<li><a href="#">Women’s Watches</a></li>
<li><a href="#">Premium Watches</a></li>
<li><a href="#">Deals on Watches</a></li>
</ul>

		</div>
	</div>
</div></div></div><div class="wpb_column vc_column_container vc_col-sm-6"><div class="vc_column-inner "><div class="wpb_wrapper">
	<div class="wpb_text_column wpb_content_element ">
		<div class="wpb_wrapper">
			<ul>
<li class="nav-title">Eyewear</li>
<li><a href="#">Men’s Sunglasses</a></li>
<li><a href="#">Women’s Sunglasses</a></li>
<li><a href="#">Spectacle Frames</a></li>
<li><a href="#">All Sunglasses</a></li>
<li><a href="#">Amazon Fashion</a></li>
</ul>

		</div>
	</div>
</div></div></div></div>
</div></li>
</ul>
</li>
<li id="menu-item-3100" class="yamm-tfw menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-has-children animate-dropdown menu-item-3100 dropdown"><a title="Car, Motorbike &amp; Industrial" href="https://demo2.chethemes.com/electro/product-category/car-electronic-gps/" data-toggle="dropdown" class="dropdown-toggle" aria-haspopup="true">Car, Motorbike &amp; Industrial</a>
<ul role="menu" class=" dropdown-menu">
	<li id="menu-item-3179" class="menu-item menu-item-type-post_type menu-item-object-static_block animate-dropdown menu-item-3179"><div class="yamm-content"><div class="vc_row wpb_row vc_row-fluid bg-yamm-content"><div class="wpb_column vc_column_container vc_col-sm-12"><div class="vc_column-inner "><div class="wpb_wrapper">
	<div class="wpb_single_image wpb_content_element vc_align_left">

		<figure class="wpb_wrapper vc_figure">
			<div class="vc_single_image-wrapper   vc_box_border_grey"><img width="540" height="523" src="https://demo2.chethemes.com/electro/wp-content/uploads/2016/03/megamenu-9.png" class="vc_single_image-img attachment-full" alt="" srcset="https://demo2.chethemes.com/electro/wp-content/uploads/2016/03/megamenu-9.png 540w, https://demo2.chethemes.com/electro/wp-content/uploads/2016/03/megamenu-9-300x291.png 300w" sizes="(max-width: 540px) 100vw, 540px"></div>
		</figure>
	</div>
</div></div></div></div><div class="vc_row wpb_row vc_row-fluid"><div class="wpb_column vc_column_container vc_col-sm-6"><div class="vc_column-inner "><div class="wpb_wrapper">
	<div class="wpb_text_column wpb_content_element ">
		<div class="wpb_wrapper">
			<ul>
<li class="nav-title">Car &amp; Motorbike</li>
<li><a href="#">All Cars &amp; Bikes</a></li>
<li><a href="#">Car &amp; Bike Care</a></li>
<li><a href="#">Lubricants</a></li>
<li class="nav-divider"></li>
<li class="nav-title">Shop for Bike</li>
<li><a href="#">Helmets &amp; Gloves</a></li>
<li><a href="#">Bike Parts</a></li>
<li class="nav-title">Shop for Car</li>
<li><a href="#">Air Fresheners</a></li>
<li><a href="#">Car Parts</a></li>
<li><a href="#">Tyre Accessories</a></li>
</ul>

		</div>
	</div>
</div></div></div><div class="wpb_column vc_column_container vc_col-sm-6"><div class="vc_column-inner "><div class="wpb_wrapper">
	<div class="wpb_text_column wpb_content_element ">
		<div class="wpb_wrapper">
			<ul>
<li class="nav-title">Industrial Supplies</li>
<li><a href="#">All Industrial Supplies</a></li>
<li><a href="#">Lab &amp; Scientific</a></li>
<li><a href="#">Janitorial &amp; Sanitation Supplies</a></li>
<li><a href="#">Test, Measure &amp; Inspect</a></li>
</ul>

		</div>
	</div>
</div></div></div></div>
</div></li>
</ul>
</li>
<li id="menu-item-3101" class="menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-has-children animate-dropdown menu-item-3101 dropdown"><a title="Accessories" href="https://demo2.chethemes.com/electro/product-category/laptops-computers/accessories-laptops-computers/" data-toggle="dropdown" class="dropdown-toggle" aria-haspopup="true">Accessories</a>
<ul role="menu" class=" dropdown-menu">
	<li id="menu-item-3113" class="menu-item menu-item-type-taxonomy menu-item-object-product_cat animate-dropdown menu-item-3113"><a title="Cases" href="https://demo2.chethemes.com/electro/product-category/accessories/cases/">Cases</a></li>
	<li id="menu-item-3114" class="menu-item menu-item-type-taxonomy menu-item-object-product_cat animate-dropdown menu-item-3114"><a title="Chargers" href="https://demo2.chethemes.com/electro/product-category/accessories/chargers/">Chargers</a></li>
	<li id="menu-item-3115" class="menu-item menu-item-type-taxonomy menu-item-object-product_cat animate-dropdown menu-item-3115"><a title="Headphone Accessories" href="https://demo2.chethemes.com/electro/product-category/accessories/headphone-accessories/">Headphone Accessories</a></li>
	<li id="menu-item-3116" class="menu-item menu-item-type-taxonomy menu-item-object-product_cat animate-dropdown menu-item-3116"><a title="Headphone Cases" href="https://demo2.chethemes.com/electro/product-category/accessories/headphone-cases/">Headphone Cases</a></li>
	<li id="menu-item-3117" class="menu-item menu-item-type-taxonomy menu-item-object-product_cat animate-dropdown menu-item-3117"><a title="Headphones" href="https://demo2.chethemes.com/electro/product-category/accessories/headphones/">Headphones</a></li>
	<li id="menu-item-3118" class="menu-item menu-item-type-taxonomy menu-item-object-product_cat animate-dropdown menu-item-3118"><a title="Computer Accessories" href="https://demo2.chethemes.com/electro/product-category/laptops-computers/accessories-laptops-computers/">Computer Accessories</a></li>
	<li id="menu-item-3119" class="menu-item menu-item-type-taxonomy menu-item-object-product_cat animate-dropdown menu-item-3119"><a title="Laptop Accessories" href="https://demo2.chethemes.com/electro/product-category/smart-phones-tablets/accessories-smart-phones-tablets/">Laptop Accessories</a></li>
</ul>
</li>
</ul>			</div>
		</div>
						</div>
			</div>

	</header>