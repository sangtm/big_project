<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

Route::get('/', function () {
	return view('index');
});

Auth::routes();

Route::get('/home', 'HomeController@index');
Route::get('auth/facebook', 'Auth\RegisterController@redirectToProvider')->name('login_with_facebook');
Route::get('auth/facebook/callback', 'Auth\RegisterController@handleProviderCallback');

Route::get('/layouts/app', function () {
	return view('layouts.app');
});
Route::get('/layouts/master', function () {
	return view('layouts.master');
});
Route::get('/shop/cart', function () {
	return view('shop.cart');
})->name('view_cart');
Route::get('/favorites/wishlist', function () {
	return view('favorites.wishlist');
})->name('wishlist');
Route::get('/shop/sell-account', function () {
	return view('shop.sell-account');
})->name('sell_account');
Route::get('/conditions/condition', function () {
	return view('conditions.condition');
})->name('condition');
